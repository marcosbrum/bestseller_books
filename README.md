# bestseller_books
This is a beginner's project. Here I perform and Exploratory Data Analysis on a set of bestselling books from a very large bookstore and apply a classification estimator to predict the genres of the books from their titles.

The dataset explored in this project contains a list of the 50 bestselling books at Amazon in every year from 2009 to 2019. It informs, besides the titles of the books, their genres, author names, price, year when it figured among the bestselling ones, number of reviews and mean rating. Unfortunately these features do not allow me to estimate the number of copies of each book that were sold each year (there are some websites which claim to provide such an estimate, but they didn't seem to be reliable enough). Making this estimate would be a project on its own (an interesting project indeed, but I'll leave it to the future).

The classification allowed me to predict the genres of the books based on their titles. I was able to predict correctly 90% of the genres of the tested books.

On the techincal side, the estimator used was the RandomForestClassifier (RFC), using the "Gini impurity" as criterion. The RFC is a decision-tree algorithm and the Gini impurity gives a measure of the likelihood of misclassification at each decision.

This dataset was obtained from Kaggle at https://www.kaggle.com/sootersaalu/amazon-top-50-bestselling-books-2009-2019.
